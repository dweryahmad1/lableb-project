$(document).ready(function () {  
  new WOW().init();
  
  $(".dropbtn").click(function(){
    $(".hidethis").toggle();
  });
  // services animation
  var y = 0;
  function editClasses() {
    if(i > 7) {
      i = 0;
    }
    list[k].style['display'] = "none"; 
    listContent[k].style['display'] = "none"; 
    listContentLinks[k].classList.remove("links-link-add-hover");
    listContentLinks[k].classList.remove("links-link-add-hover-after");
    list[i].classList.add("show-it");
    listContent[i].classList.add("show-it");
    listContentIcons[i].classList.add("links-icon-add-hover");
    listContentLinks[i].classList.add("links-link-add-hover");
    listContentLinks[i].classList.add("links-link-add-hover-after");
    var x = i;
    y = i;
    setTimeout(() => {
      list[x].classList.remove("show-it");
      listContent[x].classList.remove("show-it");
      listContentIcons[x].classList.remove("links-icon-add-hover");
      listContentLinks[x].classList.remove("links-link-add-hover");
      listContentLinks[x].classList.remove("links-link-add-hover-after");
    }, 2500);
    i++;
  }
  var intervals = [];
  var list = $('.links__text');
  var listContent = $('.services-icons-text');
  var listContentIcons = $('.links__icon');
  var listContentLinks = $('.links__link');
  var i = 0;
  var k = 0;
  var myInterval = setInterval(editClasses, 2500);
  intervals.push(myInterval);
  $(".links__link").mouseover(function(){
    intervals.forEach(clearInterval);
    list[k].style['display'] = "none"; 
    listContent[k].style['display'] = "none"; 
    listContentLinks[k].classList.remove("links-link-add-hover");
    listContentLinks[k].classList.remove("links-link-add-hover-after");
    list[y].classList.remove("show-it");
    listContent[y].classList.remove("show-it");
    listContentIcons[y].classList.remove("links-icon-add-hover");
    listContentLinks[y].classList.remove("links-link-add-hover");
    listContentLinks[y].classList.remove("links-link-add-hover-after");
    
    $( this ).find(".links__text").css("display", "block");
    $( this ).find(".services-icons-text").css("display", "block");
  });

  $(".links__link").mouseout(function(){
    for ( var j = 0; j < list.length; j += 1) {
      if ( list[j] === $( this ).find(".links__text")[0]) {
        i = j;
        k = j;
      }
    }
    i++;
    listContentLinks[k].classList.add("links-link-add-hover");
    listContentLinks[k].classList.add("links-link-add-hover-after");
    var myInterval2 = setInterval(editClasses, 2500);
    intervals.push(myInterval2);
  });
  // show nav
  $(window).scroll(function() {
    var navbar = document.querySelector(".navbar");
    var navbarBg = document.querySelector(".nav-bg-select");
    var dropbtn = document.querySelector(".dropbtn");
    var navLink = $(".navbar .nav-link");
    if($(window).scrollTop() > 800) {
      navbar.classList.add("navbar-make-fixed");
      navbarBg.classList.add("nav-bg-fixed");
      dropbtn.classList.add("dropbtn-white");
      for ( var s = 0; s <= 2; s += 1) {
        navLink[s].classList.add("nav-link-white");
      }
    }
    if($(window).scrollTop() < 800) {
      navbar.classList.remove("navbar-make-fixed");
      navbarBg.classList.remove("nav-bg-fixed");
      dropbtn.classList.remove("dropbtn-white");
      for ( var s = 0; s <= 2; s += 1) {
        navLink[s].classList.remove("nav-link-white");
      }
    }
  });
  // btn animation
  $(function() {  
    $('.btn-lern')
      .on('mouseenter', function(e) {
        var parentOffset = $(this).offset(),
            relX = e.pageX - parentOffset.left,
            relY = e.pageY - parentOffset.top;
        $(this).find('.span-hovering').css({top:relY, left:relX, "z-index": 1});
      })
      .on('mouseout', function(e) {
        var parentOffset = $(this).offset(),
            relX = e.pageX - parentOffset.left,
            relY = e.pageY - parentOffset.top;
        $(this).find('.span-hovering').css({top:relY, left:relX});
      });
  });
  // use slider
  $('.main-gallery').flickity({
    cellAlign: 'left',
    contain: true,
    prevNextButtons: false,
    lazyLoad: true,
    pageDots: false
  });
  // btn go up
  mybutton = document.getElementById("btn-go-top");
  window.onscroll = function() {scrollFunction()};
  function scrollFunction() {
    if (document.body.scrollTop > 800 || document.documentElement.scrollTop > 800) {
      mybutton.style.display = "block";
    } else {
      mybutton.style.display = "none";
    }
  }
  $('#btn-go-top').on('click', function () {
    console.log('done');
    document.body.scrollTop = 0; 
    document.documentElement.scrollTop = 0;
  });
});


